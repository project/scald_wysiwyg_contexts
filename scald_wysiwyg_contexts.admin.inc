<?php
/**
 * Implements admim forms.
 */
function scald_wysiwyg_contexts_admin_form($form, &$form_state) {
    foreach (scald_types() as $scald) {
        $contexts = array();
        foreach (scald_contexts_public() as $id => $context) {
            if (isset($context['parseable']) && $context['parseable']) {
                $contexts[$id] = t($context['title']);
            }
        }

        $form['scald_wysiwyg_contexts_allowed_contexts_' . $scald->type] =  array(
            '#title' => t('Allowed ' . $scald->type . ' contexts'),
            '#type' => 'checkboxes',
            '#options' => $contexts,
            '#default_value' => variable_get('scald_wysiwyg_contexts_allowed_contexts_' . $scald->type, array()),
            '#required' => FALSE,
        );
    }

    return system_settings_form($form);
}
